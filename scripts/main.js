// cruff, eromero4
console.log('entered main.js');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getInfoFromForm;

function getInfoFromForm(){
    console.log('entered getInfoFromForm');
    var inputName = document.getElementById("name-text").value;
    console.log('Name entered: ' + inputName);
    makeNetworkCallToIPApi(inputName);

}

function makeNetworkCallToIPApi(name){
    console.log('entered makeNetworkCallToIPAddrApi');
    var xhr = new XMLHttpRequest(); // creating request object
    var url = "https://api.ipify.org/?format=json";
    xhr.open("GET", url, true);

    xhr.onload = function(e) { // onload is triggered when the response is received
        console.log(xhr.responseText);
        updateIPWithResponse(name, xhr.responseText);
    }
    xhr.onerror = function(e) { // onerror is triggered when error response is received
        console.error(xhr.statusText);
    }
    xhr.send(null);

}

function updateIPWithResponse(name, response_text){
  var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json['ip'] == null){
        label1.innerHTML = 'Apologies, we could not find your IP address.'
    } else{
        label1.innerHTML =  name + ', your IP address is ' + response_json['ip'];
        var ip = response_json['ip'];
        makeNetworkCallToGeo(ip);
    }
} // end of updateIPWithResponse

function makeNetworkCallToGeo(IPaddr){
    console.log('entered make nw call' + IPaddr);
    var xhr = new XMLHttpRequest();
    var url = "https://ipinfo.io/"+ IPaddr + "/geo";
    xhr.open("GET", url, true)

    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateGeoInfoWithResponse(IPaddr, xhr.responseText);
    }

    xhr.onerror = function(e) { // onerror is triggered when error response is received
        console.error(xhr.statusText);
    }
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateGeoInfoWithResponse(IP, response_text){
    var response_json = JSON.parse(response_text);
    // dynamically adding label
    label_item = document.createElement("label");
    label_item.setAttribute("id", "dynamic-label" );

    var item_text = document.createTextNode("Your computer is currently in " + response_json['city'] + ', ' + response_json['country']);
    label_item.appendChild(item_text);

    // sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);

}
